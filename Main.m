clc;
clear all;
close all;

global T_CK;
global CO2Price;
global FuelCost;
T_CK            = 273.15;

% Decision variables: pr.comp_out_in, IzEf.comp, pr.comb_dp, T(4), IzEf.turb.
InitialValues   = [15, 0.87, 0.96, 1400, 0.89];

LowerBoundaries = [ 8, 0.85, 0.91, 1200, 0.86];
UpperBoundaries = [20, 0.89, 0.99, 1600, 0.91];

% Optimization
FuelCost     = 0.000004;     % $/kJ
CO2Price     = 0.050;        % $/kg

OptimizedDecisionVariables1 = fmincon(@CostsComponentsFuelPollution, InitialValues, [], [], [], [], LowerBoundaries, UpperBoundaries, @Constrains);
[T, p, pr, th, ec, mf, IzEf] = DependentVariables(OptimizedDecisionVariables1);
AnnualCost = CostsComponentsFuelPollution(OptimizedDecisionVariables1);

% Sensitivity analysis for Co2Price
FuelCost     = 0.000004;                                                                % $/kJ
CO2PriceArray = [0.025 0.030 0.035 0.040 0.045 0.050 0.055 0.060 0.065 0.070 0.075];    % $/kg

ArrayLength = size(CO2PriceArray, 2);
AnnualCostArray = zeros(ArrayLength);

for i = 1 : ArrayLength
    CO2Price = CO2PriceArray(i);
    
    OptimizedDecisionVariables = fmincon(@CostsComponentsFuelPollution, InitialValues, [], [], [], [], LowerBoundaries, UpperBoundaries, @Constrains);
    AnnualCostArray(i) = CostsComponentsFuelPollution(OptimizedDecisionVariables);
end

figure
AnnualCostArray = AnnualCostArray / 10^6;
plot(CO2PriceArray, AnnualCostArray);
grid on
title('Optimal annual cost sensitivity on price of CO2 emissions');
xlabel('Price of CO2 emissions, $/kg');
ylabel('Annual costs, mln $');

% Sensitivity analysis for FuelCost
CO2Price      = 0.050;                                                                  % $/kg
FuelCostArray = [0.0000025 0.0000030 0.0000035 0.0000040 0.0000045 0.0000050 0.0000055 0.0000060 0.0000065 0.0000070 0.0000075];    % $/kJ

ArrayLength = size(FuelCostArray, 2);
AnnualCostArray = zeros(ArrayLength);

for i = 1 : ArrayLength
    FuelCost = FuelCostArray(i);
    
    OptimizedDecisionVariables = fmincon(@CostsComponentsFuelPollution, InitialValues, [], [], [], [], LowerBoundaries, UpperBoundaries, @Constrains);
    AnnualCostArray(i) = CostsComponentsFuelPollution(OptimizedDecisionVariables);
end

figure
AnnualCostArray = AnnualCostArray / 10^6;
FuelCostArray = FuelCostArray * 10^6;
plot(FuelCostArray, AnnualCostArray);
grid on
title('Optimal annual cost sensitivity on fuel costs');
xlabel('Fuel cost, x10^-^6 $/kJ');
ylabel('Annual costs, mln $');