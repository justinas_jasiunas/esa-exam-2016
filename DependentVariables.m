function [T, p, pr, th, ec, mf, IzEf] = DependentVariables(x)

global T_CK;

[T, p, pr, th, ec, pl] = ReadData();

pr.comp_out_in  = x(1);
IzEf.comp       = x(2);
pr.comb_dp      = x(3);
T(4)            = x(4);
IzEf.turb       = x(5);

% Compressor analysis
power_air = (th.gama_air - 1) / th.gama_air;
T2s  = T(1) * pr.comp_out_in ^ power_air;
T(2) = T(1) + (T2s - T(1)) / IzEf.comp;
p(2) = pr.comp_out_in * p(1);

% Combustor analysis
p(3) = pr.comb_dp * p(2);

% Expander (turbine) analysis
pr.exp_in_out = p(3) / p(4);

power_gas = (1 - th.gama_gas) / th.gama_gas;
T(3) = T(4) / (1 - IzEf.turb * (1 - pr.exp_in_out ^ power_gas));

% Mass flow analysis 
CO2_CH4 = 44 / 16;
mf.AF = (th.LHV - th.cp_gas * T(3)) / (th.cp_gas * T(3) - th.cp_air * T(2));

Pc_mf = mf.AF * th.cp_air * (T(2) - T(1));
Pt_mf = (1 + mf.AF) * th.cp_gas * (T(3) - T(4));
mf.fuel = pl.PowerElectr / (pl.Ef_gen * (Pt_mf - Pc_mf));

mf.air = mf.fuel * mf.AF;
mf.gas = mf.fuel + mf.air;
mf.CO2 = mf.fuel * CO2_CH4;

% Exchanger analysis
T(7) = XSteam('Tsat_p', p(7)) + T_CK;
mf.steam = (th.cp_gas * (T(4) - T(5)) / (th.cp_H2O * (T(7) - T(6)) + th.L_H2O)) * mf.air;

end