function [less, less_eq] = Constrains(x)

[T, p, pr, th, ec, mf, IzEf] = DependentVariables(x);

pr.comp_out_in  = x(1);
IzEf.comp       = x(2);
pr.comb_dp      = x(3);
T(4)            = x(4);
IzEf.turb       = x(5);

less(1) = T(1) - T(2);
less(2) = T(2) - T(3);
less(3) = T(4) - T(3);
less(4) = T(5) - T(4);
less(5) = 8.63 - mf.AF;     % AF = 8.63 for compleate combustion without access air
less_eq = [];

end