function totalCost = CostsComponentsFuelPollution(x)

global CO2Price;
global FuelCost;

[T, p, pr, th, ec, mf, IzEf] = DependentVariables(x);

costComp = (ec.c(1, 1) * mf.air) / (ec.c(1, 2) - IzEf.comp    ) * pr.comp_out_in * log(pr.comp_out_in);
costComb = (ec.c(2, 1) * mf.air) / (ec.c(2, 2) - pr.comb_dp   ) * (1 + exp(ec.c(2, 3) *T(4) - ec.c(2, 4)));
costExp  = (ec.c(2, 1) * mf.gas) / (ec.c(2, 2) - pr.exp_in_out) * (1 + exp(ec.c(3, 3) *T(4) - ec.c(3, 4)));
numerator   = ((th.cp_gas * mf.gas * (T(4) - T(5))) / ((T(4) - T(7)) - (T(5) - T(6)))) ^ 0.8;
denominator = log((T(4) - T(7)) / (T(5) - T(6)));
costHRSG = (numerator / denominator) * ec.c(4, 1) + mf.steam * ec.c(4, 2) + mf.gas ^ 1.2 * ec.c(4, 3);
costComponents = ec.CRF * ec.MaintenanceF * (costComp  + costComb + costExp + costHRSG);

costFuel = mf.fuel * th.LHV * FuelCost;

costPollution = mf.CO2 * CO2Price * ec.PollutionPenalty;

secondsPerHour = 3600;
totalCost = costComponents + ec.HoursPerYear * secondsPerHour * (costFuel + costPollution);

end