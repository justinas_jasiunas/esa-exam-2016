function [T, p, pr, th, ec, pl] = ReadData()

global T_CK;

% Temperature in degrees Kelvin
dTmin           = 5;
T               = zeros(7, 1);
T(1)            = 25 + T_CK;
T(5)            = T(1) + dTmin;
T(6)            = T(1);

% Pressure ralationships
pr.comp_out_in  = 1;
pr.comb_dp      = 0;
pr.exp_in_out   = 1;
% Pressure in bar
p               = zeros(7, 1);
p(1)            = 1;
p(4)            = p(1);
p(5)            = p(1);
p(6)            = 5;
p(7)            = p(6);

% Thermodynamic parameters in kJ/kg and kJ/kgK
th.LHV          = 50000;        % kJ/kg
th.cp_air       = 1.004;        % kJ/kgK
th.cp_gas       = 1.17;         % kJ/kgK
th.cp_H2O       = 4.2;          % kJ/kgK
th.L_H2O        = 2107.42;      % kJ/kg
th.gama_air     = 1.4;
th.gama_gas     = 1.17;

% Economic parameters
ec.PollutionPenalty = 1.2;
ec.CRF          = 0.144;
ec.HoursPerYear = 6550;
ec.MaintenanceF = 1.05;
ec.c(1,1)       = 39.5;         % $/(kg/s)
ec.c(1,2)       = 0.9;
ec.c(2,1)       = 25.6;         % $/(kg/s)
ec.c(2,2)       = 0.995;
ec.c(2,3)       = 0.018;        % 1/K
ec.c(2,4)       = 26.4;
ec.c(3,1)       = 266.3;        % $/(kg/s)
ec.c(3,2)       = 0.92;
ec.c(3,3)       = 0.036;        % 1/K
ec.c(3,4)       = 54.4;
ec.c(4,1)       = 3836.15;      % $/(kW/K)^0.8
ec.c(4,2)       = 7039.6;       % $/(kg/s)
ec.c(4,3)       = 391.9;        % $/(kg/s)^1.2

% Other parameters of the plant
pl.Ef_gen       = 0.98;
pl.PowerElectr  = 10000;        % kW
end